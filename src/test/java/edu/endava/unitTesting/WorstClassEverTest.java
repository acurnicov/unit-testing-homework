package edu.endava.unitTesting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WorstClassEverTest {
    private ElephantMessageSupplier elephantMessageSupplier = mock(ElephantMessageSupplier.class);
    private WorstClassEver worstClassEver = new WorstClassEver(elephantMessageSupplier);

    @Test
    public void testHealthyElephant() {
        when(elephantMessageSupplier.getHealthyElephantMessage()).thenReturn("This elephant is healthy");
        assertThat(worstClassEver.worstMethodEver(true, 3, 150)).isEqualTo("This elephant is healthy");
    }

    @Test
    public void testElephantEatMore() {
        when(elephantMessageSupplier.getThinElephantMessage()).thenReturn("This elephant should eat more");
        assertThat(worstClassEver.worstMethodEver(true, 3, 50)).isEqualTo("This elephant should eat more");
    }

    @Test
    public void testElephantEatLess() {
        when(elephantMessageSupplier.getThickElephantMessage()).thenReturn("This elephant should eat less");
        assertThat(worstClassEver.worstMethodEver(true, 3, 400)).isEqualTo("This elephant should eat less");
    }

    @Test
    public void testHealthyBabyElephant() {
        when(elephantMessageSupplier.getHealthyBabyElephantMessage()).thenReturn("This baby elephant is healthy");
        assertThat(worstClassEver.worstMethodEver(true, 1, 70)).isEqualTo("This baby elephant is healthy");
    }

    @Test
    public void testBabyElephantEatMore() {
        when(elephantMessageSupplier.getThinBabyElephantMessage()).thenReturn("This baby elephant should eat more");
        assertThat(worstClassEver.worstMethodEver(true, 1, 20)).isEqualTo("This baby elephant should eat more");
    }

    @Test
    public void testBabyElephantEatLess() {
        when(elephantMessageSupplier.getThickBabyElephantMessage()).thenReturn("This baby elephant should eat less");
        assertThat(worstClassEver.worstMethodEver(true, 1, 100)).isEqualTo("This baby elephant should eat less");
    }

    @Test
    public void testButtonNotPressed() {
        assertThat(worstClassEver.worstMethodEver(false, 3, 150)).isEqualTo("please press the button");
    }
}
