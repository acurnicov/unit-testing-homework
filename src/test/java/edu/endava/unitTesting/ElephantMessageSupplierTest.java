package edu.endava.unitTesting;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ElephantMessageSupplierTest {
    private ElephantMessageSupplier elephantMessageSupplier = new ElephantMessageSupplier();

    @Test
    public void testHealthyElephant() {
        assertThat(elephantMessageSupplier.getHealthyElephantMessage()).isEqualTo("This elephant is healthy");
    }

    @Test
    public void testThinElephant() {
        assertThat(elephantMessageSupplier.getThinElephantMessage()).isEqualTo("This elephant should eat more");
    }

    @Test
    public void testThickElephant() {
        assertThat(elephantMessageSupplier.getThickElephantMessage()).isEqualTo("This elephant should eat less");
    }

    @Test
    public void testHealthyBabyElephant() {
        assertThat(elephantMessageSupplier.getHealthyBabyElephantMessage()).isEqualTo("This baby elephant is healthy");
    }

    @Test
    public void testThinBabyElephant() {
        assertThat(elephantMessageSupplier.getThinBabyElephantMessage()).isEqualTo("This baby elephant should eat more");
    }

    @Test
    public void testThickBabyElephant() {
        assertThat(elephantMessageSupplier.getThickBabyElephantMessage()).isEqualTo("This baby elephant should eat less");
    }
}
