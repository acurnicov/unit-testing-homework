package edu.endava.unitTesting;

public class ElephantMessageSupplier {

    public String getHealthyElephantMessage() {
        return "This elephant is healthy";
    }

    public String getThickElephantMessage() {
        return "This elephant should eat less";
    }

    public String getThinElephantMessage() {
        return "This elephant should eat more";
    }

    public String getHealthyBabyElephantMessage() {
        return "This baby elephant is healthy";
    }

    public String getThickBabyElephantMessage() {
        return "This baby elephant should eat less";
    }

    public String getThinBabyElephantMessage() {
        return "This baby elephant should eat more";
    }
}