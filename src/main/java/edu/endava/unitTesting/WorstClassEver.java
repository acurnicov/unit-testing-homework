package edu.endava.unitTesting;

public class WorstClassEver {
    private final ElephantMessageSupplier elephantMessageSupplier;

    public WorstClassEver(final ElephantMessageSupplier elephantMessageSupplier) {
        this.elephantMessageSupplier = elephantMessageSupplier;
    }

    public String worstMethodEver(boolean isPressed, int elephantsAge, double elephantsWeight) {
        if (!isPressed) return "please press the button";

        if (elephantsAge >= 2) {
            if (elephantsWeight < 100)
                return elephantMessageSupplier.getThinElephantMessage();

            if (elephantsWeight > 300)
                return elephantMessageSupplier.getThickElephantMessage();

            return elephantMessageSupplier.getHealthyElephantMessage();
        }

        if (elephantsWeight < 50)
            return elephantMessageSupplier.getThinBabyElephantMessage();

        if (elephantsWeight > 90)
            return elephantMessageSupplier.getThickBabyElephantMessage();

        return elephantMessageSupplier.getHealthyBabyElephantMessage();
    }
}

